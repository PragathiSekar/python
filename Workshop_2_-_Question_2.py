# 2)	Write a python script to transform the data available in the Data Files using Python/Pandas to be able to fit into a typical Database table. Aggregate the sales of both the branches and load the cleansed and transformed data into a CSV file (final.csv) that can be easily loaded into a table.

# Come up with the table DDL with all the necessary fields/columns needed. (Refer to Data Files: Branch1_Sales.csv, Branch2_Sales.csv; Files contain sales information of two different Convenient store branches; As of now, data is only made available until Feb 2021)


import pandas as pd

branch1 = pd.read_csv('Documents\\Branch1_Sales.csv')

branch1['PRODUCT_TYPE'] = branch1['PRODUCT_TYPE'].str.title()

branch1

branch2 = pd.read_csv('Documents\\Branch2_Sales.csv')

branch2['PRODUCT_TYPE'] = branch2['PRODUCT_TYPE'].str.title()

branch2

branch_aggregate = pd.concat([branch1, branch2], axis=0)

branch_aggregate

merge = branch_aggregate.groupby(['PRODUCT_TYPE']).agg('sum') 

merge

v = merge.reset_index()

v

final = v.melt(id_vars=['PRODUCT_TYPE'], var_name='SOLD_DATE', value_name= 'QTY_SOLD')

final

final=final[final.columns[[1,0,2]]]

final

final.to_csv('Documents/final.csv')
