# 3)	Please write an application in python language that calls the USGS API and store the result in a relational database of your choice.

# https://earthquake.usgs.gov/fdsnws/event/1/

# i)	Please query all events that have occurred during year 2017
# ii)	Read the JSON response from the API
# iii)	Design the database objects (tables) required to store the result in a relational fashion.
# iv)	Flatten the response and store it in those objects.
# v)	Add incremental fetch design to python script


import pandas as pd 
import requests
import json

data = requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-01-01&endtime=2017-12-31&alertlevel=yellow")
data = json.loads(data.text)
a = data["features"]         
df = pd.DataFrame(a)  
b = df.properties 
c = pd.json_normalize(b)
c


import pyodbc 
server = 'OTUSDPSQL' 
database = 'B12022_Target_PSekar' 
username = 'B12022_PSekar' 
password = '' 
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()



cursor.execute('create table api(Place varchar(50), Type varchar(25), Magnitude numeric(3,1), Title varchar(50))')



for index,row in c.iterrows():
    cursor.execute("INSERT INTO dbo.api values(?,?,?,?)", row['place'],row['type'],row['cdi'],row['title'])



# vi)	Provide query/analysis to give biggest earthquake of 2017
cursor.execute('select top 1 * from dbo.api order by Magnitude desc')

cnxn.commit()

cursor.close()

c.to_csv('Documents/api.csv')


import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('Documents/api.csv')

df.plot()

plt.show()



