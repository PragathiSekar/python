#!/usr/bin/env python
# coding: utf-8

# 1)	Write the Python program for the below representation of Class (using inheritance) and create objects.

class Person:
  def __init__(self, name, designation):
    self.name = name
    self.designation = designation

  def learn(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.','I know how to learn things myself')
    
  def walk(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.','I know how to walk')
    
  def eat(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.','I know how to eat')

class Programmer(Person):
  def __init__(self, name, designation, companyName):
    super().__init__(name, designation)
    self.companyName = companyName
    Person.learn(self)
    Person.walk(self)
    Person.eat(self)

  def coding(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.',"I work at", self.companyName)
    
class Dancer(Person):
  def __init__(self, name, designation, groupName):
    super().__init__(name, designation)
    self.groupName = groupName
    Person.learn(self)
    Person.walk(self)
    Person.eat(self)

  def dancing(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.',"I belong to the band called", self.groupName)
    
class Singer(Person):
  def __init__(self, name, designation, bandName):
    super().__init__(name, designation)
    self.bandName = bandName
    Person.learn(self)
    Person.walk(self)
    Person.eat(self)

  def singing(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.',"My band is", self.bandName, " and I sing there")
    
  def playGuitar(self):
    print('I am', self.name,'.', 'I am a', self.designation,'.', "My band is", self.bandName, " and I play Guitar there")
    
a = Programmer("Pragathi", "Data Engineer", "Systech")
a.coding()


# 2)	Write a python script to transform the data available in the Data Files using Python/Pandas to be able to fit into a typical Database table. Aggregate the sales of both the branches and load the cleansed and transformed data into a CSV file (final.csv) that can be easily loaded into a table.

# Come up with the table DDL with all the necessary fields/columns needed. (Refer to Data Files: Branch1_Sales.csv, Branch2_Sales.csv; Files contain sales information of two different Convenient store branches; As of now, data is only made available until Feb 2021)


import pandas as pd

branch1 = pd.read_csv('Documents\\Branch1_Sales.csv')

branch1['PRODUCT_TYPE'] = branch1['PRODUCT_TYPE'].str.title()

branch1

branch2 = pd.read_csv('Documents\\Branch2_Sales.csv')

branch2['PRODUCT_TYPE'] = branch2['PRODUCT_TYPE'].str.title()

branch2

branch_aggregate = pd.concat([branch1, branch2], axis=0)

branch_aggregate

merge = branch_aggregate.groupby(['PRODUCT_TYPE']).agg('sum') 

merge

v = merge.reset_index()

v

final = v.melt(id_vars=['PRODUCT_TYPE'], var_name='SOLD_DATE', value_name= 'QTY_SOLD')

final

final=final[final.columns[[1,0,2]]]

final

final.to_csv('Documents/final.csv')



# 3)	Please write an application in python language that calls the USGS API and store the result in a relational database of your choice.

# https://earthquake.usgs.gov/fdsnws/event/1/

# i)	Please query all events that have occurred during year 2017
# ii)	Read the JSON response from the API
# iii)	Design the database objects (tables) required to store the result in a relational fashion.
# iv)	Flatten the response and store it in those objects.
# v)	Add incremental fetch design to python script


import pandas as pd 
import requests
import json

data = requests.get("https://earthquake.usgs.gov/fdsnws/event/1/query?format=geojson&starttime=2017-01-01&endtime=2017-12-31&alertlevel=yellow")
data = json.loads(data.text)
a = data["features"]         
df = pd.DataFrame(a)  
b = df.properties 
c = pd.json_normalize(b)
c


import pyodbc 
server = 'OTUSDPSQL' 
database = 'B12022_Target_PSekar' 
username = 'B12022_PSekar' 
password = '' 
cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()



cursor.execute('create table api(Place varchar(50), Type varchar(25), Magnitude numeric(3,1), Title varchar(50))')



for index,row in c.iterrows():
    cursor.execute("INSERT INTO dbo.api values(?,?,?,?)", row['place'],row['type'],row['cdi'],row['title'])



# vi)	Provide query/analysis to give biggest earthquake of 2017
cursor.execute('select top 1 * from dbo.api order by Magnitude desc')

cnxn.commit()

cursor.close()

c.to_csv('Documents/api.csv')


import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('Documents/api.csv')

df.plot()

plt.show()



